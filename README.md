# external_storage_k8s

Install ceph-common on the base host:     yum install ceph-common

Use Git:   https://github.com/kubernetes-incubator/external-storage/tree/rbd-provisioner-v0.1.1/ceph/rbd


###Deploy RBD Provisioner

```
cat <<EOF | kubectl create -n kube-system -f -
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: rbd-provisioner
rules:
  - apiGroups: [""]
    resources: ["persistentvolumes"]
    verbs: ["get", "list", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["persistentvolumeclaims"]
    verbs: ["get", "list", "watch", "update"]
  - apiGroups: ["storage.k8s.io"]
    resources: ["storageclasses"]
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources: ["events"]
    verbs: ["list", "watch", "create", "update", "patch"]
  - apiGroups: [""]
    resources: ["services"]
    resourceNames: ["kube-dns"]
    verbs: ["list", "get"]
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: rbd-provisioner
subjects:
  - kind: ServiceAccount
    name: rbd-provisioner
    namespace: kube-system
roleRef:
  kind: ClusterRole
  name: rbd-provisioner
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: Role
metadata:
  name: rbd-provisioner
rules:
- apiGroups: [""]
  resources: ["secrets"]
  verbs: ["get"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: rbd-provisioner
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: rbd-provisioner
subjects:
- kind: ServiceAccount
  name: rbd-provisioner
  namespace: kube-system
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: rbd-provisioner
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: rbd-provisioner
spec:
  replicas: 1
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: rbd-provisioner
    spec:
      containers:
      - name: rbd-provisioner
        image: "quay.io/external_storage/rbd-provisioner:latest"
        env:
        - name: PROVISIONER_NAME
          value: ceph.com/rbd
      serviceAccount: rbd-provisioner
EOF
```

```
⚡ kubectl get pods -l app=rbd-provisioner -n kube-system
NAME                               READY     STATUS    RESTARTS   AGE
rbd-provisioner-77d75fdc5b-mpbpn   1/1       Running   1          1m
```

###Get the Key from the ceph cluster

```
sudo ceph --cluster ceph auth get-key client.admin
```

###Create Secrets on Both Kube-Sys and Default Namespace

```
kubectl create secret generic ceph-secret \
    --type="kubernetes.io/rbd" \
    --from-literal=key='AQBwruNY/lEmCxAAKS7tzZHSforkUE85htnA/g==' \
    --namespace=kube-system
```

###Cephx should be present and run these commands on the Ceph cluster

```
sudo ceph --cluster ceph osd pool create kube 1024 1024
sudo ceph --cluster ceph auth get-or-create client.kube mon 'allow r' osd 'allow rwx pool=kube'
sudo ceph --cluster ceph auth get-key client.kube
```

###Add the new client secret on KS and Default NS

```
⚡ kubectl create secret generic ceph-secret-kube \
    --type="kubernetes.io/rbd" \
    --from-literal=key='AQC/c+dYsXNUNBAAMTEW1/WnzXdmDZIBhcw6ug==' \
    --namespace=kube-system
```

###Storage Class Creation Sample

```
⚡ cat <<EOF | kubectl create -f -
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: fast-rbd
provisioner: ceph.com/rbd
parameters:
  monitors: <monitor-1-ip>:6789, <monitor-2-ip>:6789, <monitor-3-ip>:6789
  adminId: admin
  adminSecretName: ceph-secret
  adminSecretNamespace: kube-system
  pool: kube
  userId: kube
  userSecretName: ceph-secret-kube
  userSecretNamespace: kube-system
  imageFormat: "2"
  imageFeatures: layering
EOF
```

###Creating a Claim Sample - In this a claim will automatically trigger the creation of a PV

```
⚡ cat <<EOF | kubectl create -f -
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: myclaim
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 8Gi
  storageClassName: fast-rbd
EOF
```


#Glossary


```
[root@cvim-mgmt1 ~]# kubectl get pods --all-namespaces
NAMESPACE     NAME                                                             READY     STATUS    RESTARTS   AGE
default       early-salamander-grafana-65fdbfb489-7q4v7                        1/1       Running   0          2d
default       early-salamander-grafana-65fdbfb489-ds5fx                        1/1       Running   0          2d
default       youngling-zorse-prometheus-alertmanager-68fc6858c8-p5qp2         2/2       Running   0          2d
default       youngling-zorse-prometheus-kube-state-metrics-799fd74ff6-w2x92   1/1       Running   0          2d
default       youngling-zorse-prometheus-node-exporter-vrngj                   1/1       Running   0          2d
default       youngling-zorse-prometheus-pushgateway-7cc648f47c-ljpsw          1/1       Running   0          2d
default       youngling-zorse-prometheus-server-5b99cd498c-7czlw               2/2       Running   0          2d
kube-system   calico-node-9sczw                                                2/2       Running   0          2d
kube-system   coredns-78fcdf6894-b8plj                                         1/1       Running   0          2d
kube-system   coredns-78fcdf6894-s6bq4                                         1/1       Running   0          2d
kube-system   etcd-cvim-mgmt1                                                  1/1       Running   0          2d
kube-system   kube-apiserver-cvim-mgmt1                                        1/1       Running   0          2d
kube-system   kube-controller-manager-cvim-mgmt1                               1/1       Running   0          2d
kube-system   kube-proxy-9whs2                                                 1/1       Running   0          2d
kube-system   kube-scheduler-cvim-mgmt1                                        1/1       Running   0          2d
kube-system   rbd-provisioner-cd8b54dc6-7fkgc                                  1/1       Running   0          2d
kube-system   tiller-deploy-759cb9df9-sntmd                                    1/1       Running   0          2d
```

```
[root@cvim-mgmt1 ~]# kubectl get pv
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS    CLAIM                                             STORAGECLASS   REASON    AGE
pvc-272c0d39-9209-11e8-a544-5254004a174e   8Gi        RWO            Delete           Bound     default/early-salamander-grafana                  promrbd                  2d
pvc-7acc0b92-91f8-11e8-a544-5254004a174e   2Gi        RWO            Delete           Bound     default/youngling-zorse-prometheus-alertmanager   promrbd                  2d
pvc-7acd1fd3-91f8-11e8-a544-5254004a174e   8Gi        RWO            Delete           Bound     default/youngling-zorse-prometheus-server         promrbd                  2d
pvc-d65f41e0-91ed-11e8-a544-5254004a174e   10Gi       RWO            Delete           Bound     default/claim1                                    rbd                      2d
```

```
[root@cvim-mgmt1 ~]# kubectl get pvc
NAME                                      STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
claim1                                    Bound     pvc-d65f41e0-91ed-11e8-a544-5254004a174e   10Gi       RWO            rbd            2d
early-salamander-grafana                  Bound     pvc-272c0d39-9209-11e8-a544-5254004a174e   8Gi        RWO            promrbd        2d
youngling-zorse-prometheus-alertmanager   Bound     pvc-7acc0b92-91f8-11e8-a544-5254004a174e   2Gi        RWO            promrbd        2d
youngling-zorse-prometheus-server         Bound     pvc-7acd1fd3-91f8-11e8-a544-5254004a174e   8Gi        RWO            promrbd        2d
```

###Specific to RHEL Based Kubernertes Install

* Disable Selinux and Swap (remove in fstab and reboot)
```
$ setenforce 0
$ sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
```

###Installation of Helm

```
$ curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh
$ chmod 700 get_helm.sh
$ ./get_helm.sh

RBAC:   https://github.com/helm/helm/blob/master/docs/rbac.md    ---first option
```

###Desription PV and PVC

```
[root@cvim-mgmt1 ~]# kubectl describe pvc youngling-zorse-prometheus-server
Name:          youngling-zorse-prometheus-server
Namespace:     default
StorageClass:  promrbd
Status:        Bound
Volume:        pvc-7acd1fd3-91f8-11e8-a544-5254004a174e
Labels:        app=prometheus
               chart=prometheus-7.0.0
               component=server
               heritage=Tiller
               release=youngling-zorse
Annotations:   control-plane.alpha.kubernetes.io/leader={"holderIdentity":"81d1a5e0-91e8-11e8-8cea-9ea600fb8002","leaseDurationSeconds":15,"acquireTime":"2018-07-27T23:55:03Z","renewTime":"2018-07-27T23:55:05Z","lea...
               pv.kubernetes.io/bind-completed=yes
               pv.kubernetes.io/bound-by-controller=yes
               volume.beta.kubernetes.io/storage-provisioner=ceph.com/rbd
Finalizers:    [kubernetes.io/pvc-protection]
Capacity:      8Gi
Access Modes:  RWO
Events:        <none>
```

```
[root@cvim-mgmt1 ~]# kubectl describe pv pvc-7acd1fd3-91f8-11e8-a544-5254004a174e
Name:            pvc-7acd1fd3-91f8-11e8-a544-5254004a174e
Labels:          <none>
Annotations:     pv.kubernetes.io/provisioned-by=ceph.com/rbd
                 rbdProvisionerIdentity=ceph.com/rbd
Finalizers:      [kubernetes.io/pv-protection]
StorageClass:    promrbd
Status:          Bound
Claim:           default/youngling-zorse-prometheus-server
Reclaim Policy:  Delete
Access Modes:    RWO
Capacity:        8Gi
Node Affinity:   <none>
Message:
Source:
    Type:          RBD (a Rados Block Device mount on the host that shares a pod's lifetime)
    CephMonitors:  [172.29.74.26:6789  172.29.74.27:6789  172.29.74.28:6789]
    RBDImage:      kubernetes-dynamic-pvc-7b001331-91f8-11e8-8cea-9ea600fb8002
    FSType:
    RBDPool:       kube
    RadosUser:     kube
    Keyring:       /etc/ceph/keyring
    SecretRef:     &{ceph-secret-kube }
    ReadOnly:      false
Events:            <none>
```

###Helm Prometheus and Grafana (Provide Storage Class In Values.yaml)


```
[root@cvim-mgmt1 prometheus]# grep -ri "promrbd"
values.yaml:    storageClass: "promrbd"
values.yaml:    storageClass: "promrbd"
```

```
[root@cvim-mgmt1 grafana]# grep -ri "promrbd"
values.yaml:  storageClassName: promrbd
```

###References

http://docs.ceph.com/docs/master/start/kube-helm/
https://akomljen.com/using-existing-ceph-cluster-for-kubernetes-persistent-storage/
https://github.com/kubernetes-incubator/external-storage/tree/master/ceph/rbd
https://github.com/kubernetes-incubator/external-storage/blob/rbd-provisioner-v0.1.1/ceph/rbd/claim.yaml
https://github.com/kubernetes-incubator/external-storage/blob/rbd-provisioner-v0.1.1/ceph/rbd/claim.yaml
